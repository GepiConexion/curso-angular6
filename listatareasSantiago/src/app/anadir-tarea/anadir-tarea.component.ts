import { Component, OnInit } from '@angular/core';
import { AnadirTarea } from './../models/AnadirTarea.model';

@Component({
  selector: 'app-anadir-tarea',
  templateUrl: './anadir-tarea.component.html',
  styleUrls: ['./anadir-tarea.component.css']
})
export class AnadirTareaComponent implements OnInit {
  recordatorios: AnadirTarea[];
  constructor() { 
    this.recordatorios = [];
  }

  ngOnInit(): void {
  }

  guardar(recordatorio:string, fecha:string):boolean {
    this.recordatorios.push(new AnadirTarea(recordatorio, fecha));
    return false
  }

}
