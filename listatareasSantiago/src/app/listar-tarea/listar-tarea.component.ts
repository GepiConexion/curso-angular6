import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { AnadirTarea } from '../models/AnadirTarea.model';

@Component({
  selector: 'app-listar-tarea',
  templateUrl: './listar-tarea.component.html',
  styleUrls: ['./listar-tarea.component.css']
})
export class ListarTareaComponent implements OnInit {
  @Input() recordatorio: AnadirTarea;
  @HostBinding('attr.class') cssClass = 'clearfix visible-xs';
  constructor() { }

  ngOnInit(): void {
  }

}
